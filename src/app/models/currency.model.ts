import { Injectable } from '@angular/core';
import { CurrencyService } from '../services/currency.service';

@Injectable()
export class CurrencyModel {

  currencies = [];

  constructor(private currencyService: CurrencyService) { 
    this.currencyService.getCurrencies().subscribe(currencies => this.currencies = currencies);

  }

}
