import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CurrencyModel } from '../../models/currency.model';

@Component({
  selector: 'app-payform',
  templateUrl: './payform.component.html'
})
export class PayformComponent {

  submitTried = false;

  payForm: FormGroup;

  constructor(private currencyModel: CurrencyModel) { 
    this.payForm = new FormGroup({
      payer: new FormGroup({
        firstName: new FormControl('', Validators.required),
        lastName: new FormControl('', Validators.required),
        streetName: new FormControl('', Validators.required),
        streetNumber: new FormControl('', Validators.required),
      }),
      purpose: new FormControl('', Validators.required),      
      reciever: new FormControl('', Validators.required),      
      paymentCode: new FormControl('', Validators.required),      
      currency: new FormControl('', Validators.required),      
      ammount: new FormControl('', Validators.required),      
      accountNumber: new FormControl('', Validators.required),      
      model: new FormControl('', Validators.required),      
      referenceNumber: new FormControl('', Validators.required),      
      sign: new FormControl('', Validators.requiredTrue)
    })
  }

  submit(){ 
    this.submitTried = true;
    let data = this.payForm.value;
    data.date = new Date();

    console.log(data);
  }
  
  
}
