import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { PayformComponent } from './components/payform/payform.component';
import { CurrencyService } from './services/currency.service';
import { CurrencyModel } from './models/currency.model';
import { HttpModule } from '@angular/http';

const routes:Routes = [
  {path:'', component:PayformComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    PayformComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [CurrencyService, CurrencyModel],
  bootstrap: [AppComponent]
})
export class AppModule {
  
 }
