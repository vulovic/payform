import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import "rxjs/add/operator/map";

@Injectable()
export class CurrencyService {

  constructor(private http:Http){

  }

  getCurrencies(){
    return this.http.get('http://localhost:3000/currency').map(res => {
      var currencies = [];
      var jsonCurrency = res.json();
      for(var key in jsonCurrency){
          currencies.push({symbol: key, name:jsonCurrency[key]})
      }      
      return currencies;
    });
  }

}
